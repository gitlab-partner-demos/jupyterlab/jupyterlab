# jupyterlab

## Getting started

We will use the instructions supplied in the [GitLab Jupyter Guide](https://about.gitlab.com/handbook/business-technology/data-team/platform/jupyter-guide/) to complete this setup.  The run-through will be documented here.

## Setup

This project is the primary jupyterlab project.

The rancher-desktop project will store any configuration, agents, etc. for the Rancher Desktop running on my work laptop.

## Installation

This installation will be conducted on a latest version of MacOS

1. Install Python, pip, and pipenv


```bash
brew install python3
## install successful

ln -s $(which python3) /opt/homebrew/bin/python

python -m ensurepip --upgrade
## already installed

ln -s $(which pip3) /opt/homebrew/bin/pip

pip install pipenv
## install successsful
```
2. Clone gitlab data science repo
```bash
git clone git@gitlab.com:gitlab-data/data-science.git

cd data-science
```
3. Full install
```bash
make setup-jupyter-local
## To activate this project's virtualenv, run pipenv shell.
## Alternatively, run a command inside the virtualenv with pipenv run.
## Data Science environment successfully created
```
4. Make jupyter-local
```bash
make jupyter-local
## jupyter opened in my local browser!!
```

I created a hello world jupyter notebook, committed, and pushed it to the repo.  As you can see, [GitLab renders jupyter notebooks](https://gitlab.com/gitlab-partner-demos/jupyterlab/jupyterlab/-/blob/main/data-science/HelloWorld.ipynb) like a champ.

## Using Runbooks (executable by gitlab runners)

The next step is to configure executable runbooks with the GitLab Runner against the Rancher Desktop cluster on my laptop per the [GitLab Runbooks Documentation](https://docs.gitlab.com/ee/user/project/clusters/runbooks/index.html#executable-runbooks).